package com.android.settings.biometrics.fingerprint.feature;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

/**
 * Interface for fingerprint extension functionalities.
 */
public interface IFingerprintExt extends IInterface {
    String DESCRIPTOR = "com.google.hardware.biometrics.sidefps.IFingerprintExt";

    void resumeEnroll() throws RemoteException;

    /**
     * Binder stub class for the IFingerprintExt interface.
     */
    abstract class Stub extends Binder implements IFingerprintExt {

        /**
         * Returns an instance of IFingerprintExt if available from the provided IBinder.
         *
         * @param binder the IBinder to convert
         * @return IFingerprintExt instance or null if not available
         */
        public static IFingerprintExt asInterface(IBinder binder) {
            if (binder == null) {
                return null;
            }
            IInterface query = binder.queryLocalInterface(IFingerprintExt.DESCRIPTOR);
            if (query instanceof IFingerprintExt) {
                return (IFingerprintExt) query;
            }
            return new Proxy(binder);
        }

        /**
         * Proxy implementation for IFingerprintExt.
         */
        private static class Proxy implements IFingerprintExt {
            private final IBinder remote;

            Proxy(IBinder binder) {
                this.remote = binder;
            }

            @Override
            public IBinder asBinder() {
                return remote;
            }

            @Override
            public void resumeEnroll() throws RemoteException {
                Parcel data = Parcel.obtain();
                try {
                    data.writeInterfaceToken(DESCRIPTOR);
                    if (!remote.transact(TRANSACTION_RESUME_ENROLL, data, null, IBinder.FLAG_ONEWAY)) {
                        throw new RemoteException("Method resumeEnroll is unimplemented.");
                    }
                } finally {
                    data.recycle();
                }
            }

            private static final int TRANSACTION_RESUME_ENROLL = IBinder.FIRST_CALL_TRANSACTION + 1;
        }
    }
}
