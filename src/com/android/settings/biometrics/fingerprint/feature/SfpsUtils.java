package com.android.settings.biometrics.fingerprint.feature;

import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import com.android.settings.biometrics.fingerprint.feature.IFingerprintExt;

import java.util.function.Supplier;

public abstract class SfpsUtils {

    private static final String TAG = "BiometricUtil";
    private static final String FINGERPRINT_SERVICE = "android.hardware.biometrics.fingerprint.IFingerprint/default";

    /**
     * Resumes fingerprint enrollment using the IFingerprintExt interface.
     */
    public static void resumeEnroll() {
        IFingerprintExt fingerprintExt = getFingerprintExtSupplier().get();

        if (fingerprintExt == null) {
            Log.e(TAG, "Failed to connect to the fingerprint extension");
            return;
        }

        try {
            fingerprintExt.resumeEnroll();
        } catch (RemoteException e) {
            Log.e(TAG, "Failed to resume enrollment due to RemoteException", e);
        }
    }

    /**
     * Provides a supplier for the IFingerprintExt interface.
     *
     * @return Supplier of IFingerprintExt
     */
    private static Supplier<IFingerprintExt> getFingerprintExtSupplier() {
        return () -> getFingerprintExtInstance();
    }

    /**
     * Retrieves an instance of IFingerprintExt.
     *
     * @return IFingerprintExt instance or null if unavailable
     */
    private static IFingerprintExt getFingerprintExtInstance() {
        try {
            IBinder serviceBinder = ServiceManager.waitForDeclaredService(FINGERPRINT_SERVICE);
            if (serviceBinder == null) {
                Log.e(TAG, "Unable to get fingerprint service");
                return null;
            }

            return IFingerprintExt.Stub.asInterface(serviceBinder.getExtension());
        } catch (RemoteException e) {
            Log.e(TAG, "Error accessing fingerprint extension", e);
            return null;
        }
    }
}
